package server

import (
	"context"
	"fmt"
	"gitlab.com/generalistdev/go-bbs/api/v1"
	"google.golang.org/grpc"
)

var _ api.BBSServiceServer = (*bbsServer)(nil)

func NewBBSServer(opts ...grpc.ServerOption) (*grpc.Server, error) {
	gsrv := grpc.NewServer(opts...)
	srv, err := newBBSServer()
	if err != nil {
		return nil, err
	}
	api.RegisterBBSServiceServer(gsrv, srv)
	return gsrv, nil
}

type bbsServer struct {
	api.UnimplementedBBSServiceServer
}

func newBBSServer() (srv *bbsServer, err error){
	srv = &bbsServer{}
	return srv, nil
}

func (s *bbsServer) SendMessage(ctx context.Context, msg *api.MessageRequest) (*api.MessageResponse, error) {
	fmt.Printf("Message: %s\n", msg.Content)
	return &api.MessageResponse{
		Id: "1",
	}, nil
}