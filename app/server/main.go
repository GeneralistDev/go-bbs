package main

import (
	"gitlab.com/generalistdev/go-bbs/internal/server"
	"log"
	"net"
)

func main() {
	//serverTLSConfig, err := config.SetupTLSConfig(config.TLSConfig{
	//	CertFile:      "",
	//	KeyFile:       "",
	//	CAFile:        "",
	//	ServerAddress: ":0",
	//	Server:        true,
	//})
	//
	//serverCreds := credentials.NewTLS(serverTLSConfig)

	l, err := net.Listen("tcp", ":9000")
	if err != nil {
		log.Panicln(err)
	}

	srv, err := server.NewBBSServer()
	if err != nil {
		log.Panicln(err)
	}

	srv.Serve(l)
}
