module gitlab.com/generalistdev/go-bbs

go 1.16

require (
	github.com/golang/protobuf v1.4.3
	github.com/grpc/grpc-web v0.0.0-20210123023428-6b046c2fabc6 // indirect
	github.com/jroimartin/gocui v0.4.0
	github.com/nsf/termbox-go v0.0.0-20210114135735-d04385b850e8 // indirect
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.0
)
